﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using AForge.Imaging.Filters;
using AForge.Imaging;

namespace IMAVD_TRAB01_1171493_1140268
{
    public partial class Form1 : Form
    {
        Bitmap mainBitmap;
        OpenFileDialog open = new OpenFileDialog();
        private Color actualColor;
        Boolean cKeySearch = false;
        bool isPickingTextArea = false;
        string color;
        ToolTip hintCopy = new ToolTip();

        //For saving
        string savePath = null;
        private ImageFormat saveFormat;

        //Buttons bgs
        Boolean eyeDrop = false;
        Boolean colorFilter = false;
        Boolean britCon = false;
        Boolean gamma = false;
        Boolean drop = false;


        //For image history
        public List<Bitmap> history = new List<Bitmap>();
        public int index;
        private bool _selecting;
        private Rectangle _selection;

        //For Cropping
        bool isSelecting = false;
        int origX = 0;
        int origY = 0;

        //Colours
        Color aqua = Color.FromArgb(4, 185, 144);
        Color darkGray = Color.FromArgb(51, 51, 51);

        //Rullers
        Point SLoc = Point.Empty, CPoint = Point.Empty, PCloc = new Point(128, 128), SCloc = new Point(128, 128), rulerLoc, Cpos = new Point(43, 41);
        //For zoom
        bool isZoomed = false;
        Bitmap unzoomedImg;

        long score = 0;
        private bool isSearching4Col;

        public Form1()
        {
            InitializeComponent();
            comboBoxZoom.SelectedIndex = 1;
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(keypressed);
            this.MouseMove += new MouseEventHandler(normalMove);
            mainPicBox.MouseClick += new MouseEventHandler(mousePressedOnPb);
            mainPicBox.MouseDown += new MouseEventHandler(mainPicBox_MouseDown);
            mainPicBox.Paint += new PaintEventHandler(mainPicBox_Paint);
            mainPicBox.MouseUp += new MouseEventHandler(mainPicBox_MouseUp);
            tabControlBot.SelectTab(tabPageBot2);
            menuStrip1.Renderer = new ToolStripProfessionalRenderer(new menuColors());

        }

        private void normalMove(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void mousePressedOnPb(object sender, MouseEventArgs e)
        {
            if (cKeySearch)
            {
                Bitmap bmp = new Bitmap((System.Drawing.Image)history.ElementAt(index).Clone());
                Color c = bmp.GetPixel(e.X, e.Y);
                for (int i = 0; i < bmp.Height; i++)
                {
                    for (int j = 0; j < bmp.Width; j++)
                    {
                        //Get the color at each pixel
                        Color now_color = bmp.GetPixel(j, i);
                        if (isColorDarkerThan(now_color, Color.FromArgb((int)(c.R * 1.4) > 255 ? 255 : (int)(c.R * 1.4), (int)(c.G * 1.4) > 255 ? 255 : (int)(c.G * 1.4), (int)(c.B * 1.4) > 255 ? 255 : (int)(c.B * 1.4))) && isColorBrighterThan(now_color, Color.FromArgb((int)(c.R * 0.55), (int)(c.G * 0.55), (int)(c.B * 0.55))))
                        {
                            bmp.SetPixel(j, i, Color.FromArgb(0, c.R, c.G, c.B));
                        }

                    }
                }
                updateImg(bmp);
            }

            if (isPickingTextArea)
            {
                InsertText form = new InsertText(mainPicBox);

                if (form.ShowDialog() == DialogResult.OK)
                {
                    updateImg(InsertText(form.DisplayText, e.X, e.Y, form.DisplayTextFont, form.DisplayTextFontSize, form.DisplayTextFontStyle, form.DisplayTextForeColor1, form.DisplayTextForeColor2));
                    this.Invalidate();

                }
            }

            if (isSearching4Col)
            {
                Bitmap bmp = new Bitmap((System.Drawing.Image)history.ElementAt(index).Clone());
                detectObj(bmp.GetPixel(e.X, e.Y));
                isSearching4Col = false;
            }
        }

        private bool isColorDarkerThan(Color a, Color b)
        {
            return (a.R <= b.R && a.G <= b.G && a.B <= b.B);
        }

        private bool isColorBrighterThan(Color a, Color b)
        {
            return (a.R >= b.R && a.G >= b.G && a.B >= b.B);
        }

        private void keypressed(Object o, KeyEventArgs e)
        {
            // The keypressed method uses the KeyChar property to check 
            // whether the ENTER key is pressed. 

            // If the ENTER key is pressed, the Handled property is set to true, 
            // to indicate the event is handled.

            if (e.KeyCode == Keys.Z && e.Modifiers == Keys.Control && index > 0)
            {
                index--;
                mainPicBox.Image = history.ElementAt(index);
                if (isZoomed)
                {
                    unzoomedImg = history.ElementAt(index);
                    zoomPic();
                }
                mainPicBox.Refresh();
            }

            if (e.KeyCode == Keys.Y && e.Modifiers == Keys.Control && index < history.Count - 1)
            {
                index++;
                mainPicBox.Image = history.ElementAt(index);
                if (isZoomed)
                {
                    unzoomedImg = history.ElementAt(index);
                    zoomPic();
                }
                mainPicBox.Refresh();
            }

            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                if (savePath == null)
                {
                    saveImg();
                }
                else
                {
                    unzoomedImg.Save(savePath, saveFormat);
                }
            }
        }




        private void btnFlipH_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            Bitmap bmp = (Bitmap)history.ElementAt(index).Clone();
                bmp.RotateFlip(RotateFlipType.Rotate180FlipX);
                updateImg(bmp);
                zoomPic();
            
        }

        private void clearOldHistory()
        {
            if (index + 1 < history.Count)
                history.RemoveRange(index + 1, history.Count - 1);
        }

        private void btnFlipV_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            Bitmap bmp = (Bitmap)history.ElementAt(index).Clone();
                bmp.RotateFlip(RotateFlipType.Rotate180FlipY);
                updateImg(bmp);
                zoomPic();
            
        }

        private void btnRot45_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            Bitmap bmp = RotateImage((Bitmap)history.ElementAt(index).Clone(), 45);
                updateImg(bmp);
                zoomPic();
        }

        private void btnEyedrop_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            if (eyeDrop)
            {
                eyeDrop = false;
                btnEyedrop.BackgroundImage = Properties.Resources.icon_eyedrop;
            }
            else
            {
                eyeDrop = true;
                btnEyedrop.BackgroundImage = Properties.Resources.icon_eyedrop_ac;
            }
        }

        private void mainPicBox_MouseMove(object sender, MouseEventArgs e)
        {
            //Eyedrop
            if (eyeDrop)
            {
                searchColor(e);
            }

            //Rulers
            if (e.Button != System.Windows.Forms.MouseButtons.Middle)
            {
                CPoint = new Point(e.X, e.Y);
                //ajusts 
                rulerLoc = new Point(e.X + (mainPanel.Left - 70), e.Y + (mainPanel.Top - 60));
                picBoxRulerTop.Invalidate(); picBoxRulerSide.Invalidate();

            }


            if (isSelecting)
            {
                this.Cursor = Cursors.Cross;
                // Update the actual size of the selection:
                if (_selecting)
                {
                    _selection.Width = e.X - origX;
                    _selection.Height = e.Y - origY;

                    if (_selection.Height < 0)
                    {
                        _selection.Height = -_selection.Height;
                        _selection.Y = e.Y;
                    }

                    if (_selection.Width < 0)
                    {
                        _selection.Width = -_selection.Width;
                        _selection.X = e.X;
                    }

                    // Redraw the picturebox:
                    mainPicBox.Refresh();
                }
            }
        }
        public void searchColor(MouseEventArgs e)
        {
            try
            {
                if (e.X < history.ElementAt(index).Width || e.Y < history.ElementAt(index).Height)
                {
                    Bitmap bmp = new Bitmap(history.ElementAt(index));
                    Color c = bmp.GetPixel(e.X, e.Y);
                    panelEyedrop.BackColor = c;

                    //alpha
                    aVal.Text = c.A.ToString();
                    //rgb
                    rVal.Text = c.R.ToString();
                    gVal.Text = c.G.ToString();
                    bVal.Text = c.B.ToString();
                    //hue
                    hVal.Text = Math.Round(c.GetHue(), 2).ToString();
                    //Brightness
                    briVal.Text = Math.Round(c.GetBrightness(), 2).ToString();
                    //saturation
                    sVal.Text = Math.Round(c.GetSaturation(), 2).ToString();

                    //count pixels
                    lblPCount.Text = CountPixels(bmp, c).ToString();

                    color = string.Format("{0:X8}", panelEyedrop.BackColor.ToArgb());
                    color = color.Remove(color.IndexOf("x") + 1, 2);
                    hexVal.Text = "#" + color;
                }
            }
            catch (ArgumentOutOfRangeException ex) { }
        }

        private int CountPixels(Bitmap bmp, Color target_color)
        {
            int matches = 0;
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    if (bmp.GetPixel(x, y) == target_color) matches++;
                }
            }
            return matches;
        }


        private void mainPicBox_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText("#" + color);
            hintCopy.ToolTipIcon = ToolTipIcon.Info;
            hintCopy.Show("Copied to clipboard", this.mainPicBox, 3000);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (mainPicBox.Image != null)
            {
                zoomPic();
            }
            else
            {
                comboBoxZoom.SelectedIndex = 1;
            }
        }

        private void zoomPic()
        {
            double v;
            if (comboBoxZoom.SelectedIndex != 0)
            {
                v = comboBoxZoom.SelectedIndex;
            }
            else
            {
                v = .5;
            }

            if (comboBoxZoom.SelectedIndex != 1 && !isZoomed)
            {
                isZoomed = true;
                unzoomedImg = history.ElementAt(index);
            }

            if (comboBoxZoom.SelectedIndex == 1 && isZoomed)
            {
                isZoomed = false;
            }

            Bitmap bmp = new Bitmap(history.ElementAt(index), (int)(history.ElementAt(0).Width * v), (int)(history.ElementAt(0).Height * v));
            Graphics g = Graphics.FromImage(bmp);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            mainPicBox.Image = bmp;
        }



        private void brightnessBar_Scroll(object sender, EventArgs e)
        {
            brightnessLbl.Text = brightnessBar.Value.ToString();
        }

        private void contrastBar_Scroll(object sender, EventArgs e)
        {
            contrastLbl.Text = contrastBar.Value.ToString();
        }

        public static Bitmap AdjustBrightness(System.Drawing.Image Image, int Value)
        {
            //gamma -> brightness( input ) = input^gamma

            Bitmap TempBitmap = new Bitmap(Image);

            Bitmap NewBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics NewGraphics = Graphics.FromImage(NewBitmap);

            float FinalValue = (float)Value / 255.0f;

            float[][] FloatColorMatrix ={

                    new float[] {1, 0, 0, 0, 0},

                    new float[] {0, 1, 0, 0, 0},

                    new float[] {0, 0, 1, 0, 0},

                    new float[] {0, 0, 0, 1, 0},

                    new float[] {FinalValue, FinalValue, FinalValue, 1, 1}
                };

            ColorMatrix NewColorMatrix = new ColorMatrix(FloatColorMatrix);

            ImageAttributes Attributes = new ImageAttributes();
            Attributes.SetColorMatrix(NewColorMatrix);

            NewGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, Attributes);

            Attributes.Dispose();

            NewGraphics.Dispose();

            return NewBitmap;
        }


        public static Bitmap AdjustGamma(System.Drawing.Image Image, int Value)
        {
            //gamma -> brightness( input ) = input^gamma

            Bitmap TempBitmap = new Bitmap(Image);

            Bitmap NewBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);

            Graphics NewGraphics = Graphics.FromImage(NewBitmap);

            float FinalValue = (float)Value / 255.0f;

            float[][] FloatColorMatrix ={

                    new float[] {1, 0, 0, 0, 0},

                    new float[] {0, 1, 0, 0, 0},

                    new float[] {0, 0, 1, 0, 0},

                    new float[] {0, 0, 0, 1, 0},

                    new float[] {FinalValue, FinalValue, FinalValue, 1, 1}
                };

            ColorMatrix NewColorMatrix = new ColorMatrix(FloatColorMatrix);

            double gamma = Math.Pow(FinalValue, 2);

            ImageAttributes Attributes = new ImageAttributes();
            Attributes.SetColorMatrix(NewColorMatrix);

            NewGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, Attributes);

            Attributes.Dispose();

            NewGraphics.Dispose();

            return NewBitmap;
        }

        public Bitmap AdjustContrast(Bitmap Image, float Value)
        {
            Value = (100.0f + Value) / 100.0f;
            Value *= Value;
            Bitmap NewBitmap = (Bitmap)Image.Clone();
            BitmapData data = NewBitmap.LockBits(
                new Rectangle(0, 0, NewBitmap.Width, NewBitmap.Height),
                ImageLockMode.ReadWrite,
                NewBitmap.PixelFormat);
            int Height = NewBitmap.Height;
            int Width = NewBitmap.Width;

            unsafe
            {
                for (int y = 0; y < Height; ++y)
                {
                    byte* row = (byte*)data.Scan0 + (y * data.Stride);
                    int columnOffset = 0;
                    for (int x = 0; x < Width; ++x)
                    {
                        byte B = row[columnOffset];
                        byte G = row[columnOffset + 1];
                        byte R = row[columnOffset + 2];

                        float Red = R / 255.0f;
                        float Green = G / 255.0f;
                        float Blue = B / 255.0f;
                        Red = (((Red - 0.5f) * Value) + 0.5f) * 255.0f;
                        Green = (((Green - 0.5f) * Value) + 0.5f) * 255.0f;
                        Blue = (((Blue - 0.5f) * Value) + 0.5f) * 255.0f;

                        int iR = (int)Red;
                        iR = iR > 255 ? 255 : iR;
                        iR = iR < 0 ? 0 : iR;
                        int iG = (int)Green;
                        iG = iG > 255 ? 255 : iG;
                        iG = iG < 0 ? 0 : iG;
                        int iB = (int)Blue;
                        iB = iB > 255 ? 255 : iB;
                        iB = iB < 0 ? 0 : iB;

                        row[columnOffset] = (byte)iB;
                        row[columnOffset + 1] = (byte)iG;
                        row[columnOffset + 2] = (byte)iR;

                        columnOffset += 4;
                    }
                }
            }

            NewBitmap.UnlockBits(data);

            return NewBitmap;
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            updateImg(AdjustBrightness(AdjustContrast(history.ElementAt(index), contrastBar.Value), brightnessBar.Value));

                //Reset values
                brightnessBar.Value = 0;
                brightnessLbl.Text = "0";

                contrastBar.Value = 0;
                contrastLbl.Text = "0";

            
        }

        private void negBtn_Click(object sender, EventArgs e)
        {

            //Cancel operation
            if (!hasImage()) return;


            Bitmap bmp = new Bitmap((System.Drawing.Image)history.ElementAt(index).Clone());
            //negative
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    //get pixel value
                    Color p = bmp.GetPixel(x, y);

                    //extract ARGB value from p
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    //find negative value
                    r = 255 - r;
                    g = 255 - g;
                    b = 255 - b;

                    //set new ARGB value in pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }
            }
            updateImg(bmp);
        }

        private void ckeyBtn_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            if (cKeySearch)
            {
                cKeySearch = false;
            }
            else
            {
                cKeySearch = true;
            }
        }

        private void btnRotCCW45_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            Bitmap bmp = RotateImage((Bitmap)history.ElementAt(index).Clone(), -45);

               updateImg(bmp);
           
        }

        public Bitmap RotateImage(Bitmap bmp, float rotationAngle)
        {

            Bitmap rotatedImage = new Bitmap(bmp.Width, bmp.Height);
            using (Graphics g = Graphics.FromImage(rotatedImage))
            {
                // Set the rotation point to the center in the matrix
                g.TranslateTransform(bmp.Width / 2, bmp.Height / 2);
                // Rotate
                g.RotateTransform(rotationAngle);
                // Restore rotation point in the matrix
                g.TranslateTransform(-bmp.Width / 2, -bmp.Height / 2);
                // Draw the image on the bitmap
                g.DrawImage(bmp, new Point(0, 0));
            }
            
            //return the image
            return rotatedImage;
        }

        private void numericUpDownRot_ValueChanged(object sender, EventArgs e)
        {
            if (mainPicBox.Image != null)
            {
                Bitmap bmp = RotateImage((Bitmap)history.ElementAt(index).Clone(), (float)numericUpDownRot.Value);
                updateImg(bmp);
            }
            else
            {
                numericUpDownRot.Value = 0;
            }
          
        }

        private void btnApplyCFilter_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            //history
            updateImg(ColorFilter());


                //Reset values
                tbFilterRed.Value = 0;
                label15.Text = "0";
                tbFilterGreen.Value = 0;
                label14.Text = "0";
                tbFilterBlue.Value = 0;
                label13.Text = "0";
            
        }

        public Bitmap ColorFilter()
        {
            Bitmap TempBitmap = new Bitmap((Bitmap)history.ElementAt(index).Clone());

            for (int i = 0; i < TempBitmap.Height; i++)
            {
                for (int j = 0; j < TempBitmap.Width; j++)
                {
                    //Get the color at each pixel
                    Color now_color = TempBitmap.GetPixel(j, i);
                    int red = (now_color.R - tbFilterRed.Value) < 0 ? 0 : now_color.R - tbFilterRed.Value;
                    int green = (now_color.G - tbFilterGreen.Value) < 0 ? 0 : now_color.G - tbFilterGreen.Value;
                    int blue = (now_color.B - tbFilterBlue.Value) < 0 ? 0 : now_color.B - tbFilterBlue.Value;

                    TempBitmap.SetPixel(j, i, Color.FromArgb(red, green, blue));

                }
            }

            return TempBitmap;
        }

        private void tbFilterRed_Scroll(object sender, EventArgs e)
        {
            label15.Text = tbFilterRed.Value.ToString();
        }

        private void tbFilterGreen_Scroll(object sender, EventArgs e)
        {
            label14.Text = tbFilterGreen.Value.ToString();

        }

        private void tbFilterBlue_Scroll(object sender, EventArgs e)
        {
            label13.Text = tbFilterBlue.Value.ToString();

        }

        private void cropBtn_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            isSelecting = !isSelecting;
        }

        private void mainPicBox_MouseDown(object sender, MouseEventArgs e)
        {
            // Starting point of the selection:
            if (e.Button == MouseButtons.Left)
            {
                _selecting = true;
                origX = e.X;
                origY = e.Y;
                _selection = new Rectangle(new Point(e.X, e.Y), new Size());
            }
        }

        private void mainPicBox_Paint(object sender, PaintEventArgs e)
        {
            if (_selecting)
            {
                // Draw a rectangle displaying the current selection
                Pen pen = Pens.GreenYellow;


                e.Graphics.DrawRectangle(pen, _selection);
            }
        }

        private void mainPicBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left &&
                _selecting &&
                _selection.Size != new Size())
            {
                // Create cropped image:
                System.Drawing.Image img = Crop(history.ElementAt(index));
                _selection = new Rectangle();
                // Fit image to the picturebox:
                updateImg(new Bitmap(Fit2PictureBox(img, mainPicBox)));


                _selecting = false;
            }
            else
                _selecting = false;
        }

        public System.Drawing.Image Crop(System.Drawing.Image image)
        {
            try
            {
                Bitmap bmp = image as Bitmap;

                // Check if it is a bitmap:
                if (bmp == null)
                    throw new ArgumentException("No valid bitmap");

                GraphicsUnit unit = GraphicsUnit.Pixel;


                if (_selection.Right >= history.ElementAt(index).GetBounds(ref unit).Right)
                {
                    _selection.Width -= _selection.Right - (int)history.ElementAt(index).GetBounds(ref unit).Right;
                }
                // Crop the image:
                Bitmap cropBmp = bmp.Clone(_selection, bmp.PixelFormat);

                return cropBmp;
            }
            catch (OutOfMemoryException ex)
            {
                return image;
            }
        }

        public static System.Drawing.Image Fit2PictureBox(System.Drawing.Image image, PictureBox picBox)
        {
            Bitmap bmp = null;
            Graphics g;

            // Scale:
            double scaleY = (double)image.Width / picBox.Width;
            double scaleX = (double)image.Height / picBox.Height;
            double scale = scaleY < scaleX ? scaleX : scaleY;

            // Create new bitmap:
            bmp = new Bitmap(
                (int)((double)image.Width / scale),
                (int)((double)image.Height / scale));

            // Set resolution of the new image:
            bmp.SetResolution(
                image.HorizontalResolution,
                image.VerticalResolution);

            // Create graphics:
            g = Graphics.FromImage(bmp);

            // Set interpolation mode:
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // Draw the new image:
            g.DrawImage(
                image,
                new Rectangle(            // Destination
                    0, 0,
                    bmp.Width, bmp.Height),
                new Rectangle(            // Source
                    0, 0,
                    image.Width, image.Height),
                GraphicsUnit.Pixel);

            // Release the resources of the graphics:
            g.Dispose();

            // Release the resources of the origin image:
            image.Dispose();

            return bmp;
        }

        private void btnOKGamma_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            updateImg(AdjustGamma((float)redGamma.Value, (float)greenGamma.Value, (float)blueGamma.Value));

            //Reset values
            redGamma.Value = 0.2M;
            greenGamma.Value = 0.2M;
            blueGamma.Value = 0.2M;

        }

        public Bitmap AdjustGamma(double red, double green, double blue)
        {
   
            Bitmap temp = new Bitmap(history.ElementAt(index));
            Bitmap bmp = (Bitmap)temp.Clone();
            Color c;
            byte[] redGamma = CreateGammaArray(red);
            byte[] greenGamma = CreateGammaArray(green);
            byte[] blueGamma = CreateGammaArray(blue);
            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    c = bmp.GetPixel(i, j);
                    bmp.SetPixel(i, j, Color.FromArgb(redGamma[c.R], greenGamma[c.G], blueGamma[c.B]));
                }
            }
            return (Bitmap)bmp.Clone();
        }

        private byte[] CreateGammaArray(double color)
        {
            byte[] gammaArray = new byte[256];
            for (int i = 0; i < 256; ++i)
            {
                //gamma formula
                gammaArray[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, 1.0 / color)) + 0.5));
            }
            return gammaArray;
        }

        private void btnGrayScale_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            updateImg(setGrayScale());
        }

        public Bitmap setGrayScale()
        {
            Bitmap temp = new Bitmap(history.ElementAt(index));
            Bitmap bmp = (Bitmap)temp.Clone();
            Color c;

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    c = bmp.GetPixel(i, j);
                    byte gray = (byte)(.299 * c.R + .587 * c.G + .114 * c.B);

                    //50 shades of gray
                    bmp.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            return (Bitmap)bmp.Clone();
        }



        private void cutImg(int cutType)
        {
            switch (cutType)
            {
                case 0: //Top / Bottom
                    Bitmap bm = new Bitmap(history.ElementAt(index).Width, history.ElementAt(index).Height + 5);
                    Bitmap temp = new Bitmap(history.ElementAt(index));
                    for (int i = 0; i < history.ElementAt(index).Width; i++)
                    {
                        for (int j = 0; j < history.ElementAt(index).Height / 2; j++)
                        {
                            bm.SetPixel(i, j, temp.GetPixel(i, j));
                        }
                    }

                    for (int i = 0; i < history.ElementAt(index).Width; i++)
                    {
                        for (int j = (history.ElementAt(index).Height / 2); j < history.ElementAt(index).Height; j++)
                        {
                            bm.SetPixel(i, j + 5, temp.GetPixel(i, j));
                        }
                    }

                    updateImg(bm);
                    break;
                case 1: //Top Left Corner
                    Bitmap bm2 = new Bitmap(history.ElementAt(index).Width, history.ElementAt(index).Height);
                    Bitmap temp2 = new Bitmap(history.ElementAt(index));
                    for (int i = 0; i < history.ElementAt(index).Width; i++)
                    {
                        for (int j = i; j > -1; j--)
                        {

                            bm2.SetPixel(i - j, j >= mainPicBox.Image.Height ? mainPicBox.Image.Height - 1 : j, temp2.GetPixel(i - j, j >= mainPicBox.Image.Height ? mainPicBox.Image.Height - 1 : j));

                        }
                    }

                    updateImg(bm2);
                    break;
                case 2: //Bottom Right Corner
                    Bitmap bm3 = new Bitmap(history.ElementAt(index), history.ElementAt(index).Width, history.ElementAt(index).Height);
                    Bitmap temp3 = new Bitmap(history.ElementAt(index));
                    int o = 0;
                    int p = 0;
                    for (o = 0; o < history.ElementAt(index).Width; o++)
                    {
                        for (p = o; p > -1; p--)
                        {

                            bm3.SetPixel(o - p, p, Color.FromArgb(0, 0, 0, 0));

                        }
                    }

                    updateImg(bm3);
                    break;
                case 3: //Quad Cut
                    Bitmap bm4 = new Bitmap(history.ElementAt(index).Width + 5, history.ElementAt(index).Height + 5);
                    Bitmap temp4 = new Bitmap(history.ElementAt(index));
                    for (int i = 0; i < history.ElementAt(index).Width / 2; i++)
                    {
                        for (int j = 0; j < history.ElementAt(index).Height / 2; j++)
                        {
                            bm4.SetPixel(i, j, temp4.GetPixel(i, j));
                        }
                    }

                    for (int i = history.ElementAt(index).Width / 2; i < history.ElementAt(index).Width; i++)
                    {
                        for (int j = 0; j < history.ElementAt(index).Height / 2; j++)
                        {
                            bm4.SetPixel(i + 5, j, temp4.GetPixel(i, j));
                        }
                    }

                    for (int i = 0; i < history.ElementAt(index).Width / 2; i++)
                    {
                        for (int j = (history.ElementAt(index).Height / 2); j < history.ElementAt(index).Height; j++)
                        {
                            bm4.SetPixel(i, j + 5, temp4.GetPixel(i, j));
                        }
                    }

                    for (int i = history.ElementAt(index).Width / 2; i < history.ElementAt(index).Width; i++)
                    {
                        for (int j = (history.ElementAt(index).Height / 2); j < history.ElementAt(index).Height; j++)
                        {
                            bm4.SetPixel(i + 5, j + 5, temp4.GetPixel(i, j));
                        }
                    }

                    updateImg(bm4);
                    break;
            }
        }

        private void updateImg(Bitmap bm)
        {
            clearOldHistory();
            history.Add(bm);
            index = history.Count - 1;
            mainPicBox.Image = history.ElementAt(history.Count - 1);
            unzoomedImg = history.ElementAt(history.Count - 1);
            if (isZoomed)
            {
                zoomPic();
            }
            mainPicBox.Refresh();
        }

        private void btnAddText_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            isPickingTextArea = !isPickingTextArea;
            if (cKeySearch)
            {
                cKeySearch = false;
            }
        }

        public Bitmap InsertText(string text, int xPosition, int yPosition, string fontName, float fontSize, string fontStyle, string colorName1, string colorName2)
        {
            Bitmap temp = new Bitmap(history.ElementAt(index));
            Bitmap bmp = (Bitmap)temp.Clone();
            Graphics gr = Graphics.FromImage(bmp);

            if (string.IsNullOrEmpty(fontName))
                fontName = "Times New Roman";
            if (fontSize.Equals(null))
                fontSize = 10.0F;
            Font font = new Font(fontName, fontSize);
            if (!string.IsNullOrEmpty(fontStyle))
            {
                FontStyle fStyle = FontStyle.Regular;
                switch (fontStyle.ToLower())
                {
                    case "bold":
                        fStyle = FontStyle.Bold;
                        break;
                    case "italic":
                        fStyle = FontStyle.Italic;
                        break;
                    case "underline":
                        fStyle = FontStyle.Underline;
                        break;
                    case "strikeout":
                        fStyle = FontStyle.Strikeout;
                        break;

                }
                font = new Font(fontName, fontSize, fStyle);
            }
            if (string.IsNullOrEmpty(colorName1))
                colorName1 = "Black";
            if (string.IsNullOrEmpty(colorName2))
                colorName2 = colorName1;
            Color color1 = Color.FromName(colorName1);
            Color color2 = Color.FromName(colorName2);
            int gW = (int)(text.Length * fontSize);
            gW = gW == 0 ? 10 : gW;
            LinearGradientBrush LGBrush = new LinearGradientBrush(new Rectangle(0, 0, gW, (int)fontSize), color1, color2, LinearGradientMode.Vertical);
            gr.DrawString(text, font, LGBrush, xPosition, yPosition);

            return (Bitmap)bmp.Clone();

        }

        private void grayscaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            updateImg(setGrayScale());

        }

        private void topBottomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(0);
        }

        private void topRightCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(1);
        }

        private void bottomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(2);
        }

        private void quadCutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(3);
        }

        private void chromaKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            if (cKeySearch)
            {
                cKeySearch = false;
            }
            else
            {
                cKeySearch = true;
            }
        }

        private void compareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            open.Title = "Open Images";
            open.Filter = "Image Files(*.jpg; *.jpeg; *.bmp; *.png)|*.jpg; *.jpeg; *.bmp; *.png";

            if (open.ShowDialog() == DialogResult.OK)
            {
                ImageCompare frm = new ImageCompare(mainPicBox, (Bitmap)Bitmap.FromFile(open.FileName), this);
                frm.Show();

            }
            open.Dispose();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            open.Title = "Open Images";
            open.Filter = "Image Files(*.jpg; *.jpeg; *.bmp; *.png)|*.jpg; *.jpeg; *.bmp; *.png";

            if (open.ShowDialog() == DialogResult.OK)
            {
                mainBitmap = (Bitmap)Bitmap.FromFile(open.FileName);
                unzoomedImg = mainBitmap;
                //savePath = open.FileName;
                string ext = System.IO.Path.GetExtension(open.FileName);
                switch (ext)
                {
                    case ".jpeg":
                    case ".jpg":
                        saveFormat = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        saveFormat = ImageFormat.Bmp;
                        break;
                    case ".png":
                        saveFormat = ImageFormat.Png;
                        break;
                }
                updateImg(mainBitmap);// new Bitmap(Fit2PictureBox(mainBitmap, mainPicBox)));                
            }
            open.Dispose();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            if (savePath == null)
            {
                saveImg();
            }
            else
            {
                unzoomedImg.Save(savePath, saveFormat);
            }
        }

        private void saveImg()
        {
            //Cancel operation
            if (!hasImage()) return;

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpeg":
                    case ".jpg":
                        saveFormat = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        saveFormat = ImageFormat.Bmp;
                        break;
                    case ".png":
                        saveFormat = ImageFormat.Png;
                        break;
                }
                savePath = sfd.FileName;
                unzoomedImg.Save(sfd.FileName, saveFormat);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void informationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;


            string fileInfo = open.FileName;
            FileInfo info = new FileInfo(fileInfo);
            double KB = info.Length / 1024d;
            System.Drawing.Image imgDim = System.Drawing.Image.FromFile(fileInfo);

            DialogResult resultOk = MessageBox.Show("Image Name: " + Path.GetFileNameWithoutExtension(fileInfo) +
                           "\n\nImage Extension: " + Path.GetExtension(fileInfo) +
                           "\n\nImage Location: " + Path.GetFullPath(fileInfo) +
                           "\n\nImage Dimension: " + imgDim.Width + " x " + imgDim.Height +
                           "\n\nImage Size: " + Math.Round(KB, 2) + "KB  " + "(" + info.Length + " bytes)" +
                           "\n\nImage Created on: " + info.CreationTime,
                           "Image Information",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Information);

        }

        private void btnInsertText_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            isPickingTextArea = !isPickingTextArea;
            if (cKeySearch)
            {
                cKeySearch = false;
            }
        }

        private void btnBriCon_Click(object sender, EventArgs e)
        {
        
          tabControlBot.SelectTab(tabPageBot1);

            btnColorF.BackgroundImage = Properties.Resources.icon_colorF;
            if (btnBriCon.BackgroundImage != Properties.Resources.icon_briCon_ac)
            {
                btnBriCon.BackgroundImage = Properties.Resources.icon_briCon_ac;
            }
        }

        private void topToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(0);
        }

        private void topLeftCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(1);
        }

        private void bottomRightCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(2);
        }

        private void quadCutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            cutImg(3);
        }

        private void picBoxRulerSide_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            for (int x = 1; x < picBoxRulerSide.Height; x += 100)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 3.0f), new Point(0, x), new Point(20, x));
            }
            for (int x = 1; x < picBoxRulerSide.Height; x += 50)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 2.0f), new Point(0, x), new Point(10, x));
            }
            for (int x = 1; x < picBoxRulerSide.Height; x += 10)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 1.0f), new Point(0, x), new Point(5, x));
            }

            g.DrawLine(new Pen(new SolidBrush(Color.Blue), 2.0f), new Point(0, rulerLoc.Y), new Point(10, rulerLoc.Y));
        }

        private void negBtn_MouseEnter(object sender, EventArgs e)
        {
            negBtn.BackgroundImage = Properties.Resources.icon_invertC_ac;
        }

        private void negBtn_MouseLeave(object sender, EventArgs e)
        {
            negBtn.BackgroundImage = Properties.Resources.icon_invertC;
        }

        private void btnRotCCW45_MouseEnter(object sender, EventArgs e)
        {
            btnRotCCW45.BackgroundImage = Properties.Resources.icon_rotateCCW_ac;
        }

        private void btnRotCCW45_MouseLeave(object sender, EventArgs e)
        {
            btnRotCCW45.BackgroundImage = Properties.Resources.icon_rotateCCW;
        }

        private void btnRotCW45_MouseEnter(object sender, EventArgs e)
        {
            btnRotCW45.BackgroundImage = Properties.Resources.icon_rotateCW_ac;
        }

        private void btnRotCW45_MouseLeave(object sender, EventArgs e)
        {
            btnRotCW45.BackgroundImage = Properties.Resources.icon_rotateCW;
        }

        private void btnFlipV_MouseEnter(object sender, EventArgs e)
        {
            btnFlipV.BackgroundImage = Properties.Resources.icon_flipV_ac;
        }

        private void btnFlipV_MouseLeave(object sender, EventArgs e)
        {
            btnFlipV.BackgroundImage = Properties.Resources.icon_flipV;
        }

        private void btnFlipH_MouseEnter(object sender, EventArgs e)
        {
            btnFlipH.BackgroundImage = Properties.Resources.icon_flipH_ac;
        }

        private void btnFlipH_MouseLeave(object sender, EventArgs e)
        {
            btnFlipH.BackgroundImage = Properties.Resources.icon_flipH;
        }

        private void btnColorF_Click(object sender, EventArgs e)
        {
        

            tabControlBot.SelectTab(tabPageBot2);

            btnBriCon.BackgroundImage = Properties.Resources.icon_briCon;
            btnGamma.BackgroundImage = Properties.Resources.icon_gamma;

            if (btnColorF.BackgroundImage != Properties.Resources.icon_colorF_ac)
            {
                btnColorF.BackgroundImage = Properties.Resources.icon_colorF_ac;
            }
        }

        private void patternToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            PatternForm form = new PatternForm();

                if (form.ShowDialog() == DialogResult.OK)
                {
                    Bitmap bm = new Bitmap(history.ElementAt(index).Width * form.NewColumns, history.ElementAt(index).Height * form.NewRows);
                    Graphics gp = Graphics.FromImage(bm);
                    for (int i = 0; i < form.NewColumns; i++)
                    {
                        for (int j = 0; j < form.NewRows; j++)
                        {
                            //gp.TranslateTransform(-bm.Width / 2, -bm.Height / 2, MatrixOrder.Append);
                            //gp.RotateTransform(angle, MatrixOrder.Append);
                            //gp.TranslateTransform(bm.Width / 2, bm.Height / 2, MatrixOrder.Append);
                            gp.DrawImage(history.ElementAt(index), new Point((history.ElementAt(index).Width) * i, history.ElementAt(index).Height * j));
                            //gp.ResetTransform();
                        }
                    }
                    gp.Dispose();

                    mainPicBox.Size = new Size(history.ElementAt(index).Width * form.NewColumns, history.ElementAt(index).Height * form.NewRows);
                    updateImg(bm);
                    this.Invalidate();
                }
            
        }

        private void btnGamma_Click(object sender, EventArgs e)
        {
            tabControlBot.SelectTab(tabPageBot3);
        }

        private void colorPalleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;


            //Showing color choice
            DialogResult IsColorChosen = colorDialog1.ShowDialog();

            if (IsColorChosen == DialogResult.OK)
            {

                verifyColorExist(colorDialog1.Color);

            }

        }

        private void verifyColorExist(Color c)
        {
            try
            {
                Boolean IsColorFound = false;
                if (mainPicBox.Image != null)
                {
                    //Converting loaded image into bitmap
                    Bitmap bmp = new Bitmap(mainPicBox.Image);

                    for (int i = 0; i < mainPicBox.Image.Height; i++)
                    {

                        for (int j = 0; j < mainPicBox.Image.Width; j++)
                        {
                            //Get the color at each pixel
                            Color now_color = bmp.GetPixel(j, i);

                            //Compare Pixel's Color ARGB property with the picked color's ARGB property 
                            if (now_color.ToArgb() == c.ToArgb())
                            {
                                IsColorFound = true;
                                MessageBox.Show("The image has the color your picked!");
                                break;
                            }
                        }
                        if (IsColorFound == true)
                        {
                            break;
                        }
                    }
                    if (IsColorFound == false)
                    {
                        MessageBox.Show("The color you picked doesn't exist in the image!");
                    }
                }
                else
                {
                    MessageBox.Show("Image not loaded");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        public void detectObj(Color col)
        {
            // create filter
            Bitmap image = new Bitmap(history.ElementAt(index));
            Bitmap image2 = new Bitmap(history.ElementAt(index));
            EuclideanColorFiltering filter = new EuclideanColorFiltering();
            // set center color and radius
            filter.CenterColor = new RGB(col);
            filter.Radius = 85;
            // apply the filter
            filter.ApplyInPlace(image);
            //mainPicBox.Image = image;
            //mainPicBox.Refresh();
            //MessageBox.Show("Filter");
            BitmapData objectsData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
            // grayscaling
            Grayscale grayscaleFilter = new Grayscale(.299, .587, .114);//0.2125, 0.7154, 0.0721);
            UnmanagedImage grayImage = grayscaleFilter.Apply(new UnmanagedImage(objectsData));

            // unlock image
            image.UnlockBits(objectsData);

            //mainPicBox.Image = grayImage.ToManagedImage();
            //mainPicBox.Refresh();
            //MessageBox.Show("Gray");

            // create filter
            Threshold threshfilter = new Threshold(10);
            image = grayImage.ToManagedImage();
            // apply the filter
            threshfilter.ApplyInPlace(image);
            //mainPicBox.Image = image;
            //mainPicBox.Refresh();
            //MessageBox.Show("thresh");

            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 10;
            blobCounter.MinHeight = 10;
            blobCounter.MaxHeight = 300;
            blobCounter.MaxWidth = 300;
            blobCounter.FilterBlobs = true;
            blobCounter.ObjectsOrder = ObjectsOrder.Size;
            blobCounter.ProcessImage(image);

            List<Point> pos = new List<Point>();
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
            if (rects.Length > 12)
            {
                foreach (Rectangle objectRect in rects)
                {
                    Point p = objectRect.Location;
                    pos.Add(p); //add start position
                    p.X = objectRect.X + objectRect.Width;
                    p.Y = objectRect.Y + objectRect.Height;
                    pos.Add(p); //add ending position
                }
                Graphics g = Graphics.FromImage(image2);
                List<Point> hull = null;

                // Get the convex hull.
                hull = MakeConvexHull(pos);//pos is collection of rectangles position

                using (Pen pen = new Pen(Color.FromArgb(160, 255, 160), 5))
                {
                    Point[] hull_points = new Point[hull.Count];
                    hull.CopyTo(hull_points);
                    g.DrawPolygon(pen, hull_points);
                    //foreach (Rectangle objectRect in rects)
                    //    g.DrawRectangle(pen, objectRect);
                }
                g.Dispose();

                updateImg(image2);
            }
            else
            {
                foreach (Rectangle objectRect in rects)
                {

                    Graphics g = Graphics.FromImage(image2);

                    using (Pen pen = new Pen(Color.FromArgb(160, 255, 160), 5))
                    {
                        g.DrawRectangle(pen, objectRect);
                    }

                    g.Dispose();
                }
                updateImg(image2);
            }
        }

        public List<Point> MakeConvexHull(List<Point> points)
        {
            // Cull.
            points = HullCull(points);

            // Find the remaining point with the smallest Y value.
            // if (there's a tie, take the one with the smaller X value.
            Point best_pt = points[0];
            foreach (Point pt in points)
            {
                if ((pt.Y < best_pt.Y) ||
                   ((pt.Y == best_pt.Y) && (pt.X < best_pt.X)))
                {
                    best_pt = pt;
                }
            }

            // Move this point to the convex hull.
            List<Point> hull = new List<Point>();
            hull.Add(best_pt);
            points.Remove(best_pt);

            // Start wrapping up the other points.
            float sweep_angle = 0;
            for (; ; )
            {
                // Find the point with smallest AngleValue
                // from the last point.
                int X = hull[hull.Count - 1].X;
                int Y = hull[hull.Count - 1].Y;
                best_pt = points[0];
                float best_angle = 3600;

                // Search the rest of the points.
                foreach (Point pt in points)
                {
                    float test_angle = AngleValue(X, Y, pt.X, pt.Y);
                    if ((test_angle >= sweep_angle) &&
                        (best_angle > test_angle))
                    {
                        best_angle = test_angle;
                        best_pt = pt;
                    }
                }

                // See if the first point is better.
                // If so, we are done.
                float first_angle = AngleValue(X, Y, hull[0].X, hull[0].Y);
                if ((first_angle >= sweep_angle) &&
                    (best_angle >= first_angle))
                {
                    // The first point is better. We're done.
                    break;
                }

                // Add the best point to the convex hull.
                hull.Add(best_pt);
                points.Remove(best_pt);

                sweep_angle = best_angle;

                // If all of the points are on the hull, we're done.
                if (points.Count == 0) break;
            }

            return hull;
        }

        private List<Point> HullCull(List<Point> points)
        {
            // Find a culling box.
            var culling_box = GetMinMaxBox(points);

            // Cull the points.
            var results =
                points.Where(
                    pt =>
                    pt.X <= culling_box.Left || pt.X >= culling_box.Right || pt.Y <= culling_box.Top
                    || pt.Y >= culling_box.Bottom).ToList();

            return results;
        }

        private RectangleF GetMinMaxBox(List<Point> points)
        {
            // Find the MinMax quadrilateral.
            Point ul = new Point(0, 0), ur = ul, ll = ul, lr = ul;
            GetMinMaxCorners(points, ref ul, ref ur, ref ll, ref lr);

            // Get the coordinates of a box that lies inside this quadrilateral.
            var xmin = ul.X;
            var ymin = ul.Y;

            var xmax = ur.X;
            if (ymin < ur.Y)
            {
                ymin = ur.Y;
            }

            if (xmax > lr.X)
            {
                xmax = lr.X;
            }
            var ymax = lr.Y;

            if (xmin < ll.X)
            {
                xmin = ll.X;
            }
            if (ymax > ll.Y)
            {
                ymax = ll.Y;
            }

            var result = new RectangleF(xmin, ymin, xmax - xmin, ymax - ymin);
            return result;
        }

        private void verifyObjectExistenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            isSearching4Col = !isSearching4Col;
                MessageBox.Show("Click on a filtering color");
            
        }

        private void insertValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            ColorPickerRGB form = new ColorPickerRGB();

            if (form.ShowDialog() == DialogResult.OK)
            {
                verifyColorExist(form.getRGBColor());
            }
        }

        private void insertValueHEXToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            var form = new ColorPickerHex();

            if (form.ShowDialog() == DialogResult.OK)
            {
                verifyColorExist(form.HexColor);


            }
        }


        private Boolean hasImage()
        {
            if (mainPicBox.Image == null)
            {
                DialogResult resultNone = MessageBox.Show("No image found, please open an image!",
                 "Image Information",
                  MessageBoxButtons.OK,
                 MessageBoxIcon.Error);

                return false;
            }

            return true;
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!hasImage()) return;
            saveImg();
        }

        private static void GetMinMaxCorners(
            List<Point> points,
            ref Point ul,
            ref Point ur,
            ref Point ll,
            ref Point lr)
        {
            // Start with the first point as the solution.
            ul = points[0];
            ur = ul;
            ll = ul;
            lr = ul;

            // Search the other points.
            foreach (var pt in points)
            {
                if (-pt.X - pt.Y > -ul.X - ul.Y)
                {
                    ul = pt;
                }
                if (pt.X - pt.Y > ur.X - ur.Y)
                {
                    ur = pt;
                }
                if (-pt.X + pt.Y > -ll.X + ll.Y)
                {
                    ll = pt;
                }
                if (pt.X + pt.Y > lr.X + lr.Y)
                {
                    lr = pt;
                }
            }
        }

        private float AngleValue(int x1, int y1, int x2, int y2)
        {
            float dx, dy, ax, ay, t;

            dx = x2 - x1;
            ax = Math.Abs(dx);
            dy = y2 - y1;
            ay = Math.Abs(dy);
            if (ax + ay == 0)
            {
                // if (the two points are the same, return 360.
                t = 360f / 9f;
            }
            else
            {
                t = dy / (ax + ay);
            }
            if (dx < 0)
            {
                t = 2 - t;
            }
            else if (dy < 0)
            {
                t = 4 + t;
            }
            return t * 90;
        }



        private void btnDrop_Click(object sender, EventArgs e)
        {
            tabControlBot.SelectTab(tabPageEyedrop);
        }

        private void picBoxRulerTop_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            for (int x = 30; x < picBoxRulerTop.Width; x += 100)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 3.0f), new Point(x, 0), new Point(x, 20));
            }
            for (int x = 30; x < picBoxRulerTop.Width; x += 50)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 2.0f), new Point(x, 0), new Point(x, 10));
            }
            for (int x = 30; x < picBoxRulerTop.Width; x += 10)
            {
                g.DrawLine(new Pen(new SolidBrush(Color.Gray), 1.0f), new Point(x, 0), new Point(x, 5));
            }

            g.DrawLine(new Pen(new SolidBrush(Color.Red), 2.0f), new Point(rulerLoc.X, 0), new Point(rulerLoc.X, 10));
        }

        private void resizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Cancel operation
            if (!hasImage()) return;

            ResizeForm form = new ResizeForm(history.ElementAt(index).Width, history.ElementAt(index).Height);

            if (form.ShowDialog() == DialogResult.OK)
            {
                var res = new Bitmap(form.NewWidth, form.NewHeight);

                using (var graphic = Graphics.FromImage(res))
                {
                    graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphic.SmoothingMode = SmoothingMode.HighQuality;
                    graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphic.CompositingQuality = CompositingQuality.HighQuality;
                    graphic.DrawImage(history.ElementAt(index), 0, 0, form.NewWidth, form.NewHeight);
                }
                updateImg(res);
                this.Invalidate();
            }

        }

    }
}