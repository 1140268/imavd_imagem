﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_TRAB01_1171493_1140268
{
    public partial class ColorPickerHex : Form
    {
        public ColorPickerHex()
        {
            InitializeComponent();
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
        }
        public Color HexColor
        {
            get
            {
                Color c;
                try
                {
                    c = ColorTranslator.FromHtml(textHex.Text);
                }
                catch (Exception e) { c = Color.Black; }
                return c;
            }
            set { }
        }

        private void textHex_TextChanged(object sender, EventArgs e)
        {
            panelColor.BackColor = HexColor;
        }

    }
}
