﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_TRAB01_1171493_1140268
{
    class menuColors : ProfessionalColorTable
    {

        public override Color MenuItemSelected
        {
            get { return Color.FromArgb(54, 185, 144);}
        }

        public override Color MenuItemSelectedGradientBegin
        {
            get { return Color.FromArgb(54, 185, 144); }
        }
        public override Color MenuItemSelectedGradientEnd
        {
            get { return Color.FromArgb(54, 185, 144); }
        }
    }
}
