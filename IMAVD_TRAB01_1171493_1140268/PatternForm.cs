using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IMAVD_TRAB01_1171493_1140268
{
    public partial class PatternForm : Form
    {
        public PatternForm()
        {
            InitializeComponent();
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
        }

        public int NewColumns
        {
            get
            {
                return Convert.ToInt32(numericUpDown1.Value);
            }
            set { Convert.ToInt32(numericUpDown1.Value); }
        }

        public int NewRows
        {
            get
            {               
                return Convert.ToInt32(numericUpDown2.Value);
            }
            set { Convert.ToInt32(numericUpDown2.Value); }
        }
    }
}