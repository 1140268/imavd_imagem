﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_TRAB01_1171493_1140268
{
    public partial class ColorPickerRGB : Form
    {
        public ColorPickerRGB()
        {
            InitializeComponent();
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
        }

        public int getR
        {
            get
            {
                if (string.IsNullOrEmpty(numericR.Text))
                    numericR.Text = "0";
                return Convert.ToInt32(numericR.Text);
            }
            set { numericR.Text = value.ToString(); }
        }

        public int getG
        {
            get
            {
                if (string.IsNullOrEmpty(numericG.Text))
                    numericG.Text = "0";
                return Convert.ToInt32(numericG.Text);
            }
            set { numericG.Text = value.ToString(); }
        }

        public int getB
        {
            get
            {
                if (string.IsNullOrEmpty(numericB.Text))
                    numericB.Text = "0";
                return Convert.ToInt32(numericB.Text);
            }
            set { numericB.Text = value.ToString(); }
        }

        public Color getRGBColor()
        {
            return Color.FromArgb(getR, getG, getB);
        }

        private void numericR_ValueChanged(object sender, EventArgs e)
        {
            updatePanelColor();
        }

        private void updatePanelColor()
        {
            panelColor.BackColor = getRGBColor();
        }

        private void numericG_ValueChanged(object sender, EventArgs e)
        {
            updatePanelColor();

        }

        private void numericB_ValueChanged(object sender, EventArgs e)
        {
            updatePanelColor();

        }
    }
}
