﻿namespace IMAVD_TRAB01_1171493_1140268
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.lblPCount = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.sVal = new System.Windows.Forms.Label();
            this.briVal = new System.Windows.Forms.Label();
            this.hVal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Hue = new System.Windows.Forms.Label();
            this.panelEyedrop = new System.Windows.Forms.Panel();
            this.hexVal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bVal = new System.Windows.Forms.Label();
            this.gVal = new System.Windows.Forms.Label();
            this.rVal = new System.Windows.Forms.Label();
            this.aVal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxZoom = new System.Windows.Forms.ComboBox();
            this.numericUpDownRot = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chromaKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.divisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topLeftCornerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bottomRightCornerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quadCutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.compareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patternToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorPalleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertValueHEXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verifyObjectExistenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControlTop = new System.Windows.Forms.TabControl();
            this.tabPageEyedrop = new System.Windows.Forms.TabPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.mainPicBox = new System.Windows.Forms.PictureBox();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.btnInsertText = new System.Windows.Forms.Button();
            this.negBtn = new System.Windows.Forms.Button();
            this.btnFlipH = new System.Windows.Forms.Button();
            this.btnFlipV = new System.Windows.Forms.Button();
            this.btnRotCW45 = new System.Windows.Forms.Button();
            this.btnEyedrop = new System.Windows.Forms.Button();
            this.cropBtn = new System.Windows.Forms.Button();
            this.btnRotCCW45 = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelBotProperties = new System.Windows.Forms.Panel();
            this.lblRot = new System.Windows.Forms.Label();
            this.lblZoom = new System.Windows.Forms.Label();
            this.panelBotRight = new System.Windows.Forms.Panel();
            this.tabControlBot = new System.Windows.Forms.TabControl();
            this.tabPageBot3 = new System.Windows.Forms.TabPage();
            this.blueGamma = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.greenGamma = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.redGamma = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnOKGamma = new System.Windows.Forms.Button();
            this.tabPageBot2 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnApplyCFilter = new System.Windows.Forms.Button();
            this.tbFilterBlue = new System.Windows.Forms.TrackBar();
            this.tbFilterGreen = new System.Windows.Forms.TrackBar();
            this.tbFilterRed = new System.Windows.Forms.TrackBar();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPageBot1 = new System.Windows.Forms.TabPage();
            this.contrastLbl = new System.Windows.Forms.Label();
            this.brightnessLbl = new System.Windows.Forms.Label();
            this.applyBtn = new System.Windows.Forms.Button();
            this.contrastBar = new System.Windows.Forms.TrackBar();
            this.brightnessBar = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnBriCon = new System.Windows.Forms.Button();
            this.btnColorF = new System.Windows.Forms.Button();
            this.btnGamma = new System.Windows.Forms.Button();
            this.btnDrop = new System.Windows.Forms.Button();
            this.picBoxRulerTop = new System.Windows.Forms.PictureBox();
            this.picBoxRulerSide = new System.Windows.Forms.PictureBox();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRot)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabControlTop.SuspendLayout();
            this.tabPageEyedrop.SuspendLayout();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPicBox)).BeginInit();
            this.leftPanel.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.panelBotProperties.SuspendLayout();
            this.panelBotRight.SuspendLayout();
            this.tabControlBot.SuspendLayout();
            this.tabPageBot3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blueGamma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenGamma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redGamma)).BeginInit();
            this.tabPageBot2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterRed)).BeginInit();
            this.tabPageBot1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contrastBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRulerTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRulerSide)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPCount
            // 
            this.lblPCount.AutoSize = true;
            this.lblPCount.Location = new System.Drawing.Point(102, 139);
            this.lblPCount.Name = "lblPCount";
            this.lblPCount.Size = new System.Drawing.Size(16, 17);
            this.lblPCount.TabIndex = 20;
            this.lblPCount.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 139);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "Pixel Count";
            // 
            // sVal
            // 
            this.sVal.AutoSize = true;
            this.sVal.Location = new System.Drawing.Point(220, 105);
            this.sVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sVal.Name = "sVal";
            this.sVal.Size = new System.Drawing.Size(16, 17);
            this.sVal.TabIndex = 27;
            this.sVal.Text = "0";
            // 
            // briVal
            // 
            this.briVal.AutoSize = true;
            this.briVal.Location = new System.Drawing.Point(220, 75);
            this.briVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.briVal.Name = "briVal";
            this.briVal.Size = new System.Drawing.Size(16, 17);
            this.briVal.TabIndex = 26;
            this.briVal.Text = "0";
            // 
            // hVal
            // 
            this.hVal.AutoSize = true;
            this.hVal.Location = new System.Drawing.Point(220, 43);
            this.hVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hVal.Name = "hVal";
            this.hVal.Size = new System.Drawing.Size(16, 17);
            this.hVal.TabIndex = 25;
            this.hVal.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(134, 105);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "Saturation";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(134, 75);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 17);
            this.label12.TabIndex = 22;
            this.label12.Text = "Brightness";
            // 
            // Hue
            // 
            this.Hue.AutoSize = true;
            this.Hue.Location = new System.Drawing.Point(134, 43);
            this.Hue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Hue.Name = "Hue";
            this.Hue.Size = new System.Drawing.Size(34, 17);
            this.Hue.TabIndex = 21;
            this.Hue.Text = "Hue";
            // 
            // panelEyedrop
            // 
            this.panelEyedrop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEyedrop.Location = new System.Drawing.Point(275, 15);
            this.panelEyedrop.Name = "panelEyedrop";
            this.panelEyedrop.Size = new System.Drawing.Size(110, 146);
            this.panelEyedrop.TabIndex = 20;
            // 
            // hexVal
            // 
            this.hexVal.AutoSize = true;
            this.hexVal.Location = new System.Drawing.Point(61, 105);
            this.hexVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hexVal.Name = "hexVal";
            this.hexVal.Size = new System.Drawing.Size(16, 17);
            this.hexVal.TabIndex = 19;
            this.hexVal.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Hex";
            // 
            // bVal
            // 
            this.bVal.AutoSize = true;
            this.bVal.Location = new System.Drawing.Point(61, 75);
            this.bVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bVal.Name = "bVal";
            this.bVal.Size = new System.Drawing.Size(16, 17);
            this.bVal.TabIndex = 17;
            this.bVal.Text = "0";
            // 
            // gVal
            // 
            this.gVal.AutoSize = true;
            this.gVal.Location = new System.Drawing.Point(61, 45);
            this.gVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gVal.Name = "gVal";
            this.gVal.Size = new System.Drawing.Size(16, 17);
            this.gVal.TabIndex = 16;
            this.gVal.Text = "0";
            // 
            // rVal
            // 
            this.rVal.AutoSize = true;
            this.rVal.Location = new System.Drawing.Point(61, 15);
            this.rVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.rVal.Name = "rVal";
            this.rVal.Size = new System.Drawing.Size(16, 17);
            this.rVal.TabIndex = 15;
            this.rVal.Text = "0";
            // 
            // aVal
            // 
            this.aVal.AutoSize = true;
            this.aVal.Location = new System.Drawing.Point(220, 15);
            this.aVal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.aVal.Name = "aVal";
            this.aVal.Size = new System.Drawing.Size(16, 17);
            this.aVal.TabIndex = 14;
            this.aVal.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "B";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "G";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "R";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Alpha";
            // 
            // comboBoxZoom
            // 
            this.comboBoxZoom.BackColor = System.Drawing.Color.White;
            this.comboBoxZoom.FormattingEnabled = true;
            this.comboBoxZoom.Items.AddRange(new object[] {
            "50%",
            "100%",
            "200%",
            "300%",
            "400%",
            "500%"});
            this.comboBoxZoom.Location = new System.Drawing.Point(54, 4);
            this.comboBoxZoom.Name = "comboBoxZoom";
            this.comboBoxZoom.Size = new System.Drawing.Size(121, 24);
            this.comboBoxZoom.TabIndex = 14;
            this.comboBoxZoom.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // numericUpDownRot
            // 
            this.numericUpDownRot.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownRot.Location = new System.Drawing.Point(259, 6);
            this.numericUpDownRot.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDownRot.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDownRot.Name = "numericUpDownRot";
            this.numericUpDownRot.Size = new System.Drawing.Size(47, 22);
            this.numericUpDownRot.TabIndex = 24;
            this.numericUpDownRot.ValueChanged += new System.EventHandler(this.numericUpDownRot_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.findColorToolStripMenuItem,
            this.verifyObjectExistenceToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1478, 28);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.informationToolStripMenuItem,
            this.toolStripMenuItem1,
            this.closeToolStripMenuItem1});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+S";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // informationToolStripMenuItem
            // 
            this.informationToolStripMenuItem.Name = "informationToolStripMenuItem";
            this.informationToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.informationToolStripMenuItem.Text = "Information";
            this.informationToolStripMenuItem.Click += new System.EventHandler(this.informationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(213, 6);
            // 
            // closeToolStripMenuItem1
            // 
            this.closeToolStripMenuItem1.Name = "closeToolStripMenuItem1";
            this.closeToolStripMenuItem1.Size = new System.Drawing.Size(216, 26);
            this.closeToolStripMenuItem1.Text = "Close";
            this.closeToolStripMenuItem1.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.chromaKeyToolStripMenuItem});
            this.editToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+Z";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.undoToolStripMenuItem.Text = "Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeyDisplayString = "CTRL+Y";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.redoToolStripMenuItem.Text = "Redo";
            // 
            // chromaKeyToolStripMenuItem
            // 
            this.chromaKeyToolStripMenuItem.Name = "chromaKeyToolStripMenuItem";
            this.chromaKeyToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.chromaKeyToolStripMenuItem.Text = "Chroma Key";
            this.chromaKeyToolStripMenuItem.Click += new System.EventHandler(this.chromaKeyToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripMenuItem,
            this.divisionToolStripMenuItem,
            this.toolStripSeparator1,
            this.compareToolStripMenuItem,
            this.resizeToolStripMenuItem,
            this.patternToolStripMenuItem});
            this.imageToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(63, 24);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grayscaleToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // grayscaleToolStripMenuItem
            // 
            this.grayscaleToolStripMenuItem.Name = "grayscaleToolStripMenuItem";
            this.grayscaleToolStripMenuItem.Size = new System.Drawing.Size(147, 26);
            this.grayscaleToolStripMenuItem.Text = "Grayscale";
            this.grayscaleToolStripMenuItem.Click += new System.EventHandler(this.grayscaleToolStripMenuItem_Click);
            // 
            // divisionToolStripMenuItem
            // 
            this.divisionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.topToolStripMenuItem,
            this.topLeftCornerToolStripMenuItem,
            this.bottomRightCornerToolStripMenuItem,
            this.quadCutToolStripMenuItem});
            this.divisionToolStripMenuItem.Name = "divisionToolStripMenuItem";
            this.divisionToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.divisionToolStripMenuItem.Text = "Divide";
            // 
            // topToolStripMenuItem
            // 
            this.topToolStripMenuItem.Name = "topToolStripMenuItem";
            this.topToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.topToolStripMenuItem.Text = "Top | Bottom";
            this.topToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // topLeftCornerToolStripMenuItem
            // 
            this.topLeftCornerToolStripMenuItem.Name = "topLeftCornerToolStripMenuItem";
            this.topLeftCornerToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.topLeftCornerToolStripMenuItem.Text = "Top | Left Corner";
            this.topLeftCornerToolStripMenuItem.Click += new System.EventHandler(this.topLeftCornerToolStripMenuItem_Click);
            // 
            // bottomRightCornerToolStripMenuItem
            // 
            this.bottomRightCornerToolStripMenuItem.Name = "bottomRightCornerToolStripMenuItem";
            this.bottomRightCornerToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.bottomRightCornerToolStripMenuItem.Text = "Bottom | Right Corner";
            this.bottomRightCornerToolStripMenuItem.Click += new System.EventHandler(this.bottomRightCornerToolStripMenuItem_Click);
            // 
            // quadCutToolStripMenuItem
            // 
            this.quadCutToolStripMenuItem.Name = "quadCutToolStripMenuItem";
            this.quadCutToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.quadCutToolStripMenuItem.Text = "Quad Cut";
            this.quadCutToolStripMenuItem.Click += new System.EventHandler(this.quadCutToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(151, 6);
            // 
            // compareToolStripMenuItem
            // 
            this.compareToolStripMenuItem.Name = "compareToolStripMenuItem";
            this.compareToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.compareToolStripMenuItem.Text = "Compare...";
            this.compareToolStripMenuItem.Click += new System.EventHandler(this.compareToolStripMenuItem_Click);
            // 
            // resizeToolStripMenuItem
            // 
            this.resizeToolStripMenuItem.Name = "resizeToolStripMenuItem";
            this.resizeToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.resizeToolStripMenuItem.Text = "Resize...";
            this.resizeToolStripMenuItem.Click += new System.EventHandler(this.resizeToolStripMenuItem_Click);
            // 
            // patternToolStripMenuItem
            // 
            this.patternToolStripMenuItem.Name = "patternToolStripMenuItem";
            this.patternToolStripMenuItem.Size = new System.Drawing.Size(154, 26);
            this.patternToolStripMenuItem.Text = "Pattern...";
            this.patternToolStripMenuItem.Click += new System.EventHandler(this.patternToolStripMenuItem_Click);
            // 
            // findColorToolStripMenuItem
            // 
            this.findColorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colorPalleteToolStripMenuItem,
            this.insertValueToolStripMenuItem,
            this.insertValueHEXToolStripMenuItem});
            this.findColorToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.findColorToolStripMenuItem.Name = "findColorToolStripMenuItem";
            this.findColorToolStripMenuItem.Size = new System.Drawing.Size(89, 24);
            this.findColorToolStripMenuItem.Text = "Find Color";
            // 
            // colorPalleteToolStripMenuItem
            // 
            this.colorPalleteToolStripMenuItem.Name = "colorPalleteToolStripMenuItem";
            this.colorPalleteToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.colorPalleteToolStripMenuItem.Text = "Color Palette";
            this.colorPalleteToolStripMenuItem.Click += new System.EventHandler(this.colorPalleteToolStripMenuItem_Click);
            // 
            // insertValueToolStripMenuItem
            // 
            this.insertValueToolStripMenuItem.Name = "insertValueToolStripMenuItem";
            this.insertValueToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.insertValueToolStripMenuItem.Text = "Insert value (RGB)";
            this.insertValueToolStripMenuItem.Click += new System.EventHandler(this.insertValueToolStripMenuItem_Click);
            // 
            // insertValueHEXToolStripMenuItem
            // 
            this.insertValueHEXToolStripMenuItem.Name = "insertValueHEXToolStripMenuItem";
            this.insertValueHEXToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.insertValueHEXToolStripMenuItem.Text = "Insert Value (HEX)";
            this.insertValueHEXToolStripMenuItem.Click += new System.EventHandler(this.insertValueHEXToolStripMenuItem_Click);
            // 
            // verifyObjectExistenceToolStripMenuItem
            // 
            this.verifyObjectExistenceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.verifyObjectExistenceToolStripMenuItem.Name = "verifyObjectExistenceToolStripMenuItem";
            this.verifyObjectExistenceToolStripMenuItem.Size = new System.Drawing.Size(133, 24);
            this.verifyObjectExistenceToolStripMenuItem.Text = "Check For Object";
            this.verifyObjectExistenceToolStripMenuItem.Click += new System.EventHandler(this.verifyObjectExistenceToolStripMenuItem_Click);
            // 
            // tabControlTop
            // 
            this.tabControlTop.Controls.Add(this.tabPageEyedrop);
            this.tabControlTop.Location = new System.Drawing.Point(-11, -31);
            this.tabControlTop.Name = "tabControlTop";
            this.tabControlTop.SelectedIndex = 0;
            this.tabControlTop.Size = new System.Drawing.Size(411, 209);
            this.tabControlTop.TabIndex = 31;
            // 
            // tabPageEyedrop
            // 
            this.tabPageEyedrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPageEyedrop.Controls.Add(this.lblPCount);
            this.tabPageEyedrop.Controls.Add(this.label10);
            this.tabPageEyedrop.Controls.Add(this.label1);
            this.tabPageEyedrop.Controls.Add(this.sVal);
            this.tabPageEyedrop.Controls.Add(this.label2);
            this.tabPageEyedrop.Controls.Add(this.briVal);
            this.tabPageEyedrop.Controls.Add(this.label3);
            this.tabPageEyedrop.Controls.Add(this.hVal);
            this.tabPageEyedrop.Controls.Add(this.label4);
            this.tabPageEyedrop.Controls.Add(this.label11);
            this.tabPageEyedrop.Controls.Add(this.aVal);
            this.tabPageEyedrop.Controls.Add(this.label12);
            this.tabPageEyedrop.Controls.Add(this.rVal);
            this.tabPageEyedrop.Controls.Add(this.Hue);
            this.tabPageEyedrop.Controls.Add(this.gVal);
            this.tabPageEyedrop.Controls.Add(this.panelEyedrop);
            this.tabPageEyedrop.Controls.Add(this.bVal);
            this.tabPageEyedrop.Controls.Add(this.hexVal);
            this.tabPageEyedrop.Controls.Add(this.label6);
            this.tabPageEyedrop.ForeColor = System.Drawing.Color.White;
            this.tabPageEyedrop.Location = new System.Drawing.Point(4, 25);
            this.tabPageEyedrop.Name = "tabPageEyedrop";
            this.tabPageEyedrop.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEyedrop.Size = new System.Drawing.Size(403, 180);
            this.tabPageEyedrop.TabIndex = 0;
            this.tabPageEyedrop.Text = "Eyedrop";
            // 
            // mainPanel
            // 
            this.mainPanel.AutoScroll = true;
            this.mainPanel.Controls.Add(this.mainPicBox);
            this.mainPanel.Location = new System.Drawing.Point(104, 67);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(957, 603);
            this.mainPanel.TabIndex = 36;
            // 
            // mainPicBox
            // 
            this.mainPicBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPicBox.Location = new System.Drawing.Point(3, 3);
            this.mainPicBox.Name = "mainPicBox";
            this.mainPicBox.Size = new System.Drawing.Size(864, 495);
            this.mainPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.mainPicBox.TabIndex = 0;
            this.mainPicBox.TabStop = false;
            this.mainPicBox.DoubleClick += new System.EventHandler(this.mainPicBox_DoubleClick);
            this.mainPicBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mainPicBox_MouseMove);
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.leftPanel.Controls.Add(this.btnInsertText);
            this.leftPanel.Controls.Add(this.negBtn);
            this.leftPanel.Controls.Add(this.btnFlipH);
            this.leftPanel.Controls.Add(this.btnFlipV);
            this.leftPanel.Controls.Add(this.btnRotCW45);
            this.leftPanel.Controls.Add(this.btnEyedrop);
            this.leftPanel.Controls.Add(this.cropBtn);
            this.leftPanel.Controls.Add(this.btnRotCCW45);
            this.leftPanel.Location = new System.Drawing.Point(0, 31);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(65, 679);
            this.leftPanel.TabIndex = 37;
            // 
            // btnInsertText
            // 
            this.btnInsertText.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_insertText;
            this.btnInsertText.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInsertText.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInsertText.FlatAppearance.BorderSize = 0;
            this.btnInsertText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsertText.Location = new System.Drawing.Point(12, 288);
            this.btnInsertText.Name = "btnInsertText";
            this.btnInsertText.Size = new System.Drawing.Size(40, 40);
            this.btnInsertText.TabIndex = 24;
            this.btnInsertText.UseVisualStyleBackColor = true;
            this.btnInsertText.Click += new System.EventHandler(this.btnInsertText_Click);
            // 
            // negBtn
            // 
            this.negBtn.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_invertC;
            this.negBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.negBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.negBtn.FlatAppearance.BorderSize = 0;
            this.negBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.negBtn.Location = new System.Drawing.Point(12, 58);
            this.negBtn.Name = "negBtn";
            this.negBtn.Size = new System.Drawing.Size(40, 40);
            this.negBtn.TabIndex = 20;
            this.negBtn.UseVisualStyleBackColor = true;
            this.negBtn.Click += new System.EventHandler(this.negBtn_Click);
            this.negBtn.MouseEnter += new System.EventHandler(this.negBtn_MouseEnter);
            this.negBtn.MouseLeave += new System.EventHandler(this.negBtn_MouseLeave);
            // 
            // btnFlipH
            // 
            this.btnFlipH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFlipH.BackgroundImage")));
            this.btnFlipH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFlipH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFlipH.FlatAppearance.BorderSize = 0;
            this.btnFlipH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFlipH.Location = new System.Drawing.Point(12, 242);
            this.btnFlipH.Name = "btnFlipH";
            this.btnFlipH.Size = new System.Drawing.Size(40, 40);
            this.btnFlipH.TabIndex = 6;
            this.btnFlipH.UseVisualStyleBackColor = true;
            this.btnFlipH.Click += new System.EventHandler(this.btnFlipH_Click);
            this.btnFlipH.MouseEnter += new System.EventHandler(this.btnFlipH_MouseEnter);
            this.btnFlipH.MouseLeave += new System.EventHandler(this.btnFlipH_MouseLeave);
            // 
            // btnFlipV
            // 
            this.btnFlipV.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_flipV;
            this.btnFlipV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFlipV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFlipV.FlatAppearance.BorderSize = 0;
            this.btnFlipV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFlipV.Location = new System.Drawing.Point(12, 196);
            this.btnFlipV.Name = "btnFlipV";
            this.btnFlipV.Size = new System.Drawing.Size(40, 40);
            this.btnFlipV.TabIndex = 7;
            this.btnFlipV.UseVisualStyleBackColor = true;
            this.btnFlipV.Click += new System.EventHandler(this.btnFlipV_Click);
            this.btnFlipV.MouseEnter += new System.EventHandler(this.btnFlipV_MouseEnter);
            this.btnFlipV.MouseLeave += new System.EventHandler(this.btnFlipV_MouseLeave);
            // 
            // btnRotCW45
            // 
            this.btnRotCW45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnRotCW45.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_rotateCW;
            this.btnRotCW45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRotCW45.FlatAppearance.BorderSize = 0;
            this.btnRotCW45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRotCW45.Location = new System.Drawing.Point(12, 150);
            this.btnRotCW45.Name = "btnRotCW45";
            this.btnRotCW45.Size = new System.Drawing.Size(40, 40);
            this.btnRotCW45.TabIndex = 8;
            this.btnRotCW45.UseVisualStyleBackColor = false;
            this.btnRotCW45.Click += new System.EventHandler(this.btnRot45_Click);
            this.btnRotCW45.MouseEnter += new System.EventHandler(this.btnRotCW45_MouseEnter);
            this.btnRotCW45.MouseLeave += new System.EventHandler(this.btnRotCW45_MouseLeave);
            // 
            // btnEyedrop
            // 
            this.btnEyedrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnEyedrop.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_eyedrop;
            this.btnEyedrop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEyedrop.FlatAppearance.BorderSize = 0;
            this.btnEyedrop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEyedrop.Location = new System.Drawing.Point(12, 12);
            this.btnEyedrop.Name = "btnEyedrop";
            this.btnEyedrop.Size = new System.Drawing.Size(40, 40);
            this.btnEyedrop.TabIndex = 12;
            this.btnEyedrop.UseVisualStyleBackColor = false;
            this.btnEyedrop.Click += new System.EventHandler(this.btnEyedrop_Click);
            // 
            // cropBtn
            // 
            this.cropBtn.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_crop;
            this.cropBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cropBtn.FlatAppearance.BorderSize = 0;
            this.cropBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cropBtn.Location = new System.Drawing.Point(12, 334);
            this.cropBtn.Name = "cropBtn";
            this.cropBtn.Size = new System.Drawing.Size(40, 40);
            this.cropBtn.TabIndex = 23;
            this.cropBtn.UseVisualStyleBackColor = true;
            this.cropBtn.Click += new System.EventHandler(this.cropBtn_Click);
            // 
            // btnRotCCW45
            // 
            this.btnRotCCW45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnRotCCW45.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_rotateCCW;
            this.btnRotCCW45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRotCCW45.FlatAppearance.BorderSize = 0;
            this.btnRotCCW45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRotCCW45.Location = new System.Drawing.Point(12, 104);
            this.btnRotCCW45.Name = "btnRotCCW45";
            this.btnRotCCW45.Size = new System.Drawing.Size(40, 40);
            this.btnRotCCW45.TabIndex = 23;
            this.btnRotCCW45.UseVisualStyleBackColor = false;
            this.btnRotCCW45.Click += new System.EventHandler(this.btnRotCCW45_Click);
            this.btnRotCCW45.MouseEnter += new System.EventHandler(this.btnRotCCW45_MouseEnter);
            this.btnRotCCW45.MouseLeave += new System.EventHandler(this.btnRotCCW45_MouseLeave);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelTop.Controls.Add(this.tabControlTop);
            this.panelTop.Location = new System.Drawing.Point(1067, 62);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(389, 164);
            this.panelTop.TabIndex = 41;
            // 
            // panelBotProperties
            // 
            this.panelBotProperties.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelBotProperties.Controls.Add(this.lblRot);
            this.panelBotProperties.Controls.Add(this.lblZoom);
            this.panelBotProperties.Controls.Add(this.numericUpDownRot);
            this.panelBotProperties.Controls.Add(this.comboBoxZoom);
            this.panelBotProperties.Location = new System.Drawing.Point(68, 677);
            this.panelBotProperties.Name = "panelBotProperties";
            this.panelBotProperties.Size = new System.Drawing.Size(993, 33);
            this.panelBotProperties.TabIndex = 45;
            // 
            // lblRot
            // 
            this.lblRot.AutoSize = true;
            this.lblRot.Location = new System.Drawing.Point(203, 7);
            this.lblRot.Name = "lblRot";
            this.lblRot.Size = new System.Drawing.Size(50, 17);
            this.lblRot.TabIndex = 25;
            this.lblRot.Text = "Rotate";
            // 
            // lblZoom
            // 
            this.lblZoom.AutoSize = true;
            this.lblZoom.Location = new System.Drawing.Point(4, 7);
            this.lblZoom.Name = "lblZoom";
            this.lblZoom.Size = new System.Drawing.Size(44, 17);
            this.lblZoom.TabIndex = 15;
            this.lblZoom.Text = "Zoom";
            // 
            // panelBotRight
            // 
            this.panelBotRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.panelBotRight.Controls.Add(this.tabControlBot);
            this.panelBotRight.Location = new System.Drawing.Point(1067, 263);
            this.panelBotRight.Name = "panelBotRight";
            this.panelBotRight.Size = new System.Drawing.Size(400, 230);
            this.panelBotRight.TabIndex = 38;
            // 
            // tabControlBot
            // 
            this.tabControlBot.Controls.Add(this.tabPageBot3);
            this.tabControlBot.Controls.Add(this.tabPageBot2);
            this.tabControlBot.Controls.Add(this.tabPageBot1);
            this.tabControlBot.Location = new System.Drawing.Point(-11, -31);
            this.tabControlBot.Name = "tabControlBot";
            this.tabControlBot.SelectedIndex = 0;
            this.tabControlBot.Size = new System.Drawing.Size(418, 305);
            this.tabControlBot.TabIndex = 30;
            // 
            // tabPageBot3
            // 
            this.tabPageBot3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPageBot3.Controls.Add(this.blueGamma);
            this.tabPageBot3.Controls.Add(this.label19);
            this.tabPageBot3.Controls.Add(this.greenGamma);
            this.tabPageBot3.Controls.Add(this.label20);
            this.tabPageBot3.Controls.Add(this.redGamma);
            this.tabPageBot3.Controls.Add(this.label21);
            this.tabPageBot3.Controls.Add(this.label22);
            this.tabPageBot3.Controls.Add(this.btnOKGamma);
            this.tabPageBot3.ForeColor = System.Drawing.Color.White;
            this.tabPageBot3.Location = new System.Drawing.Point(4, 25);
            this.tabPageBot3.Name = "tabPageBot3";
            this.tabPageBot3.Size = new System.Drawing.Size(410, 276);
            this.tabPageBot3.TabIndex = 2;
            this.tabPageBot3.Text = "Gamma";
            // 
            // blueGamma
            // 
            this.blueGamma.DecimalPlaces = 1;
            this.blueGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.blueGamma.Location = new System.Drawing.Point(87, 126);
            this.blueGamma.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.blueGamma.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.blueGamma.Name = "blueGamma";
            this.blueGamma.Size = new System.Drawing.Size(120, 22);
            this.blueGamma.TabIndex = 17;
            this.blueGamma.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 126);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 17);
            this.label19.TabIndex = 16;
            this.label19.Text = "Blue";
            // 
            // greenGamma
            // 
            this.greenGamma.DecimalPlaces = 1;
            this.greenGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.greenGamma.Location = new System.Drawing.Point(87, 89);
            this.greenGamma.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.greenGamma.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.greenGamma.Name = "greenGamma";
            this.greenGamma.Size = new System.Drawing.Size(120, 22);
            this.greenGamma.TabIndex = 15;
            this.greenGamma.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(17, 89);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 17);
            this.label20.TabIndex = 14;
            this.label20.Text = "Green";
            // 
            // redGamma
            // 
            this.redGamma.DecimalPlaces = 1;
            this.redGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.redGamma.Location = new System.Drawing.Point(87, 53);
            this.redGamma.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.redGamma.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.redGamma.Name = "redGamma";
            this.redGamma.Size = new System.Drawing.Size(120, 22);
            this.redGamma.TabIndex = 13;
            this.redGamma.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(17, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 17);
            this.label21.TabIndex = 12;
            this.label21.Text = "Red";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(17, 14);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(277, 17);
            this.label22.TabIndex = 11;
            this.label22.Text = "The values are only available from 0.2 to 5";
            // 
            // btnOKGamma
            // 
            this.btnOKGamma.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(185)))), ((int)(((byte)(144)))));
            this.btnOKGamma.FlatAppearance.BorderSize = 0;
            this.btnOKGamma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOKGamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOKGamma.Location = new System.Drawing.Point(18, 194);
            this.btnOKGamma.Name = "btnOKGamma";
            this.btnOKGamma.Size = new System.Drawing.Size(90, 32);
            this.btnOKGamma.TabIndex = 9;
            this.btnOKGamma.Text = "APPLY";
            this.btnOKGamma.UseVisualStyleBackColor = false;
            this.btnOKGamma.Click += new System.EventHandler(this.btnOKGamma_Click);
            // 
            // tabPageBot2
            // 
            this.tabPageBot2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPageBot2.Controls.Add(this.label13);
            this.tabPageBot2.Controls.Add(this.label14);
            this.tabPageBot2.Controls.Add(this.label15);
            this.tabPageBot2.Controls.Add(this.btnApplyCFilter);
            this.tabPageBot2.Controls.Add(this.tbFilterBlue);
            this.tabPageBot2.Controls.Add(this.tbFilterGreen);
            this.tabPageBot2.Controls.Add(this.tbFilterRed);
            this.tabPageBot2.Controls.Add(this.label16);
            this.tabPageBot2.Controls.Add(this.label17);
            this.tabPageBot2.Controls.Add(this.label18);
            this.tabPageBot2.ForeColor = System.Drawing.Color.White;
            this.tabPageBot2.Location = new System.Drawing.Point(4, 25);
            this.tabPageBot2.Name = "tabPageBot2";
            this.tabPageBot2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBot2.Size = new System.Drawing.Size(410, 276);
            this.tabPageBot2.TabIndex = 1;
            this.tabPageBot2.Text = "Color Filter";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(356, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 17);
            this.label13.TabIndex = 24;
            this.label13.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(356, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 17);
            this.label14.TabIndex = 23;
            this.label14.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(356, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 17);
            this.label15.TabIndex = 22;
            this.label15.Text = "0";
            // 
            // btnApplyCFilter
            // 
            this.btnApplyCFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(185)))), ((int)(((byte)(144)))));
            this.btnApplyCFilter.FlatAppearance.BorderSize = 0;
            this.btnApplyCFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplyCFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApplyCFilter.Location = new System.Drawing.Point(18, 194);
            this.btnApplyCFilter.Name = "btnApplyCFilter";
            this.btnApplyCFilter.Size = new System.Drawing.Size(90, 32);
            this.btnApplyCFilter.TabIndex = 21;
            this.btnApplyCFilter.Text = "APPLY";
            this.btnApplyCFilter.UseVisualStyleBackColor = false;
            this.btnApplyCFilter.Click += new System.EventHandler(this.btnApplyCFilter_Click);
            // 
            // tbFilterBlue
            // 
            this.tbFilterBlue.Location = new System.Drawing.Point(95, 143);
            this.tbFilterBlue.Maximum = 255;
            this.tbFilterBlue.Name = "tbFilterBlue";
            this.tbFilterBlue.Size = new System.Drawing.Size(255, 56);
            this.tbFilterBlue.TabIndex = 20;
            this.tbFilterBlue.Scroll += new System.EventHandler(this.tbFilterBlue_Scroll);
            // 
            // tbFilterGreen
            // 
            this.tbFilterGreen.Location = new System.Drawing.Point(95, 81);
            this.tbFilterGreen.Maximum = 255;
            this.tbFilterGreen.Name = "tbFilterGreen";
            this.tbFilterGreen.Size = new System.Drawing.Size(255, 56);
            this.tbFilterGreen.TabIndex = 19;
            this.tbFilterGreen.Scroll += new System.EventHandler(this.tbFilterGreen_Scroll);
            // 
            // tbFilterRed
            // 
            this.tbFilterRed.Location = new System.Drawing.Point(95, 21);
            this.tbFilterRed.Maximum = 255;
            this.tbFilterRed.Name = "tbFilterRed";
            this.tbFilterRed.Size = new System.Drawing.Size(255, 56);
            this.tbFilterRed.TabIndex = 18;
            this.tbFilterRed.Scroll += new System.EventHandler(this.tbFilterRed_Scroll);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 149);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 17);
            this.label16.TabIndex = 17;
            this.label16.Text = "Blue";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 17);
            this.label17.TabIndex = 16;
            this.label17.Text = "Green";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 17);
            this.label18.TabIndex = 15;
            this.label18.Text = "Red";
            // 
            // tabPageBot1
            // 
            this.tabPageBot1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.tabPageBot1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabPageBot1.Controls.Add(this.contrastLbl);
            this.tabPageBot1.Controls.Add(this.brightnessLbl);
            this.tabPageBot1.Controls.Add(this.applyBtn);
            this.tabPageBot1.Controls.Add(this.contrastBar);
            this.tabPageBot1.Controls.Add(this.brightnessBar);
            this.tabPageBot1.Controls.Add(this.label7);
            this.tabPageBot1.Controls.Add(this.label9);
            this.tabPageBot1.ForeColor = System.Drawing.Color.White;
            this.tabPageBot1.Location = new System.Drawing.Point(4, 25);
            this.tabPageBot1.Name = "tabPageBot1";
            this.tabPageBot1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBot1.Size = new System.Drawing.Size(410, 276);
            this.tabPageBot1.TabIndex = 0;
            this.tabPageBot1.Text = "Brightness/Contrast";
            // 
            // contrastLbl
            // 
            this.contrastLbl.AutoSize = true;
            this.contrastLbl.Location = new System.Drawing.Point(356, 87);
            this.contrastLbl.Name = "contrastLbl";
            this.contrastLbl.Size = new System.Drawing.Size(16, 17);
            this.contrastLbl.TabIndex = 23;
            this.contrastLbl.Text = "0";
            // 
            // brightnessLbl
            // 
            this.brightnessLbl.AutoSize = true;
            this.brightnessLbl.Location = new System.Drawing.Point(356, 31);
            this.brightnessLbl.Name = "brightnessLbl";
            this.brightnessLbl.Size = new System.Drawing.Size(16, 17);
            this.brightnessLbl.TabIndex = 22;
            this.brightnessLbl.Text = "0";
            // 
            // applyBtn
            // 
            this.applyBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(185)))), ((int)(((byte)(144)))));
            this.applyBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.applyBtn.FlatAppearance.BorderSize = 0;
            this.applyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.applyBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.applyBtn.Location = new System.Drawing.Point(18, 194);
            this.applyBtn.Name = "applyBtn";
            this.applyBtn.Size = new System.Drawing.Size(90, 32);
            this.applyBtn.TabIndex = 20;
            this.applyBtn.Text = "APPLY";
            this.applyBtn.UseVisualStyleBackColor = false;
            this.applyBtn.Click += new System.EventHandler(this.applyBtn_Click);
            // 
            // contrastBar
            // 
            this.contrastBar.Location = new System.Drawing.Point(95, 81);
            this.contrastBar.Maximum = 100;
            this.contrastBar.Minimum = -100;
            this.contrastBar.Name = "contrastBar";
            this.contrastBar.Size = new System.Drawing.Size(255, 56);
            this.contrastBar.TabIndex = 20;
            this.contrastBar.Scroll += new System.EventHandler(this.contrastBar_Scroll);
            // 
            // brightnessBar
            // 
            this.brightnessBar.Location = new System.Drawing.Point(95, 21);
            this.brightnessBar.Maximum = 255;
            this.brightnessBar.Minimum = -255;
            this.brightnessBar.Name = "brightnessBar";
            this.brightnessBar.Size = new System.Drawing.Size(255, 56);
            this.brightnessBar.TabIndex = 17;
            this.brightnessBar.Scroll += new System.EventHandler(this.brightnessBar_Scroll);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Brightness";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Contrast";
            // 
            // btnBriCon
            // 
            this.btnBriCon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnBriCon.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_briCon1;
            this.btnBriCon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBriCon.FlatAppearance.BorderSize = 0;
            this.btnBriCon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBriCon.Location = new System.Drawing.Point(1154, 232);
            this.btnBriCon.Name = "btnBriCon";
            this.btnBriCon.Size = new System.Drawing.Size(135, 25);
            this.btnBriCon.TabIndex = 36;
            this.btnBriCon.UseVisualStyleBackColor = false;
            this.btnBriCon.Click += new System.EventHandler(this.btnBriCon_Click);
            // 
            // btnColorF
            // 
            this.btnColorF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnColorF.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_colorF_ac;
            this.btnColorF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnColorF.FlatAppearance.BorderSize = 0;
            this.btnColorF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnColorF.Location = new System.Drawing.Point(1067, 232);
            this.btnColorF.Name = "btnColorF";
            this.btnColorF.Size = new System.Drawing.Size(81, 25);
            this.btnColorF.TabIndex = 39;
            this.btnColorF.UseVisualStyleBackColor = false;
            this.btnColorF.Click += new System.EventHandler(this.btnColorF_Click);
            // 
            // btnGamma
            // 
            this.btnGamma.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnGamma.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_gamma;
            this.btnGamma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGamma.FlatAppearance.BorderSize = 0;
            this.btnGamma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGamma.Location = new System.Drawing.Point(1295, 232);
            this.btnGamma.Name = "btnGamma";
            this.btnGamma.Size = new System.Drawing.Size(63, 25);
            this.btnGamma.TabIndex = 40;
            this.btnGamma.UseVisualStyleBackColor = false;
            this.btnGamma.Click += new System.EventHandler(this.btnGamma_Click);
            // 
            // btnDrop
            // 
            this.btnDrop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnDrop.BackgroundImage = global::IMAVD_TRAB01_1171493_1140268.Properties.Resources.icon_drop_ac;
            this.btnDrop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDrop.FlatAppearance.BorderSize = 0;
            this.btnDrop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDrop.Location = new System.Drawing.Point(1067, 31);
            this.btnDrop.Name = "btnDrop";
            this.btnDrop.Size = new System.Drawing.Size(63, 25);
            this.btnDrop.TabIndex = 42;
            this.btnDrop.UseVisualStyleBackColor = false;
            this.btnDrop.Click += new System.EventHandler(this.btnDrop_Click);
            // 
            // picBoxRulerTop
            // 
            this.picBoxRulerTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.picBoxRulerTop.Location = new System.Drawing.Point(68, 31);
            this.picBoxRulerTop.Name = "picBoxRulerTop";
            this.picBoxRulerTop.Size = new System.Drawing.Size(993, 30);
            this.picBoxRulerTop.TabIndex = 43;
            this.picBoxRulerTop.TabStop = false;
            this.picBoxRulerTop.Paint += new System.Windows.Forms.PaintEventHandler(this.picBoxRulerTop_Paint);
            // 
            // picBoxRulerSide
            // 
            this.picBoxRulerSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.picBoxRulerSide.Location = new System.Drawing.Point(68, 60);
            this.picBoxRulerSide.Name = "picBoxRulerSide";
            this.picBoxRulerSide.Size = new System.Drawing.Size(30, 611);
            this.picBoxRulerSide.TabIndex = 44;
            this.picBoxRulerSide.TabStop = false;
            this.picBoxRulerSide.Paint += new System.Windows.Forms.PaintEventHandler(this.picBoxRulerSide_Paint);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(1478, 722);
            this.Controls.Add(this.picBoxRulerTop);
            this.Controls.Add(this.panelBotProperties);
            this.Controls.Add(this.picBoxRulerSide);
            this.Controls.Add(this.btnDrop);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.btnGamma);
            this.Controls.Add(this.btnColorF);
            this.Controls.Add(this.btnBriCon);
            this.Controls.Add(this.panelBotRight);
            this.Controls.Add(this.leftPanel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.mainPanel);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Mini Photoshop";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRot)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControlTop.ResumeLayout(false);
            this.tabPageEyedrop.ResumeLayout(false);
            this.tabPageEyedrop.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainPicBox)).EndInit();
            this.leftPanel.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.panelBotProperties.ResumeLayout(false);
            this.panelBotProperties.PerformLayout();
            this.panelBotRight.ResumeLayout(false);
            this.tabControlBot.ResumeLayout(false);
            this.tabPageBot3.ResumeLayout(false);
            this.tabPageBot3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.blueGamma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenGamma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redGamma)).EndInit();
            this.tabPageBot2.ResumeLayout(false);
            this.tabPageBot2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFilterRed)).EndInit();
            this.tabPageBot1.ResumeLayout(false);
            this.tabPageBot1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.contrastBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRulerTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRulerSide)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label bVal;
        private System.Windows.Forms.Label gVal;
        private System.Windows.Forms.Label rVal;
        private System.Windows.Forms.Label aVal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label hexVal;
        private System.Windows.Forms.Panel panelEyedrop;
        private System.Windows.Forms.Label sVal;
        private System.Windows.Forms.Label briVal;
        private System.Windows.Forms.Label hVal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Hue;
        private System.Windows.Forms.ComboBox comboBoxZoom;
        private System.Windows.Forms.Label lblPCount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownRot;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControlTop;
        private System.Windows.Forms.TabPage tabPageEyedrop;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayscaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem divisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem topToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem topLeftCornerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bottomRightCornerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quadCutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chromaKeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem1;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem findColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorPalleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertValueToolStripMenuItem;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelBotProperties;
        private System.Windows.Forms.Label lblZoom;
        private System.Windows.Forms.Label lblRot;
        private System.Windows.Forms.ToolStripMenuItem resizeToolStripMenuItem;
        private System.Windows.Forms.Panel panelBotRight;
        private System.Windows.Forms.TabControl tabControlBot;
        private System.Windows.Forms.TabPage tabPageBot2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnApplyCFilter;
        private System.Windows.Forms.TrackBar tbFilterBlue;
        private System.Windows.Forms.TrackBar tbFilterGreen;
        private System.Windows.Forms.TrackBar tbFilterRed;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPageBot3;
        private System.Windows.Forms.NumericUpDown blueGamma;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown greenGamma;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown redGamma;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnOKGamma;
        private System.Windows.Forms.TabPage tabPageBot1;
        private System.Windows.Forms.Label contrastLbl;
        private System.Windows.Forms.Label brightnessLbl;
        private System.Windows.Forms.Button applyBtn;
        private System.Windows.Forms.TrackBar contrastBar;
        private System.Windows.Forms.TrackBar brightnessBar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem patternToolStripMenuItem;
        private System.Windows.Forms.PictureBox mainPicBox;
        private System.Windows.Forms.Button btnInsertText;
        private System.Windows.Forms.Button negBtn;
        private System.Windows.Forms.Button btnFlipH;
        private System.Windows.Forms.Button btnFlipV;
        private System.Windows.Forms.Button btnRotCW45;
        private System.Windows.Forms.Button btnEyedrop;
        private System.Windows.Forms.Button cropBtn;
        private System.Windows.Forms.Button btnRotCCW45;
        private System.Windows.Forms.Button btnBriCon;
        private System.Windows.Forms.Button btnColorF;
        private System.Windows.Forms.Button btnGamma;
        private System.Windows.Forms.Button btnDrop;
        private System.Windows.Forms.PictureBox picBoxRulerTop;
        private System.Windows.Forms.PictureBox picBoxRulerSide;
        private System.Windows.Forms.ToolStripMenuItem verifyObjectExistenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertValueHEXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
    }
}

