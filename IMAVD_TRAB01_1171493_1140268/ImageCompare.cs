﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAVD_TRAB01_1171493_1140268
{
    public partial class ImageCompare : Form
    {
        PictureBox parent;
        Form1 parentForm;
        OpenFileDialog open = new OpenFileDialog();

        public ImageCompare(PictureBox parent, Bitmap bitmap, Form1 form)
        {
            InitializeComponent();
            this.parent = parent;
            this.parentForm = form;
            picBoxNewPic.Image = bitmap;
            picBoxCurrent.Image = parent.Image;
            compareIMG();
        }

        private void compareIMG()
        {
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();

            // Load the images.
            Bitmap bm1 = (Bitmap)picBoxCurrent.Image;
            Bitmap bm2 = (Bitmap)picBoxNewPic.Image;

            // Make a difference image.
            int wid = Math.Min(bm1.Width, bm2.Width);
            int hgt = Math.Min(bm1.Height, bm2.Height);
            Bitmap bm3 = new Bitmap(wid, hgt);

            // Create the difference image.
            bool are_identical = true;
            Color eq_color = Color.Lime;
            Color ne_color = Color.Red;
            for (int x = 0; x < wid; x++)
            {
                for (int y = 0; y < hgt; y++)
                {
                    if (bm1.GetPixel(x, y).Equals(bm2.GetPixel(x, y)))
                        bm3.SetPixel(x, y, eq_color);
                    else
                    {
                        bm3.SetPixel(x, y, ne_color);
                        are_identical = false;
                    }
                }
            }

            // Display the result.
            picBoxRes.Image = bm3;

            this.Cursor = Cursors.Default;
            if ((bm1.Width != bm2.Width) || (bm1.Height != bm2.Height)) are_identical = false;
            if (are_identical)
                labelResult.Text = "The images are identical";
            else
                labelResult.Text = "The images are different";
            
        }

        
    }
}
