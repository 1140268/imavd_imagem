﻿namespace IMAVD_TRAB01_1171493_1140268
{
    partial class ImageCompare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picBoxRes = new System.Windows.Forms.PictureBox();
            this.picBoxCurrent = new System.Windows.Forms.PictureBox();
            this.picBoxNewPic = new System.Windows.Forms.PictureBox();
            this.labelResult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxNewPic)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 490);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Equal pixels";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Lime;
            this.panel2.Location = new System.Drawing.Point(24, 486);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(25, 25);
            this.panel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 490);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Different pixels";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(155, 486);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(25, 25);
            this.panel1.TabIndex = 6;
            // 
            // picBoxRes
            // 
            this.picBoxRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBoxRes.Location = new System.Drawing.Point(23, 23);
            this.picBoxRes.Name = "picBoxRes";
            this.picBoxRes.Size = new System.Drawing.Size(450, 450);
            this.picBoxRes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxRes.TabIndex = 1;
            this.picBoxRes.TabStop = false;
            // 
            // picBoxCurrent
            // 
            this.picBoxCurrent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBoxCurrent.Location = new System.Drawing.Point(485, 22);
            this.picBoxCurrent.Name = "picBoxCurrent";
            this.picBoxCurrent.Size = new System.Drawing.Size(220, 220);
            this.picBoxCurrent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxCurrent.TabIndex = 0;
            this.picBoxCurrent.TabStop = false;
            // 
            // picBoxNewPic
            // 
            this.picBoxNewPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBoxNewPic.Location = new System.Drawing.Point(485, 253);
            this.picBoxNewPic.Name = "picBoxNewPic";
            this.picBoxNewPic.Size = new System.Drawing.Size(220, 220);
            this.picBoxNewPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxNewPic.TabIndex = 1;
            this.picBoxNewPic.TabStop = false;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResult.Location = new System.Drawing.Point(467, 491);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(238, 20);
            this.labelResult.TabIndex = 6;
            this.labelResult.Text = "You didn\'t compare images yet";
            // 
            // ImageCompare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(727, 534);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picBoxNewPic);
            this.Controls.Add(this.picBoxCurrent);
            this.Controls.Add(this.picBoxRes);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ImageCompare";
            this.Text = "Image Comparison";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxNewPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picBoxRes;
        private System.Windows.Forms.PictureBox picBoxCurrent;
        private System.Windows.Forms.PictureBox picBoxNewPic;
        private System.Windows.Forms.Label labelResult;
    }
}